* GIPP (parameters) for MAJA
This directory corresponds to the parameters for
- MAJA 3.3.x, standalone version
- with NATIF format
- with CAMS option enabled
- for Landsat-8 satellite

For data volume reasons, the directory is not complete, and you should add the files contained in this reference :
https://zenodo.org/record/3891634/files/20200531_LUT_MAJA_LANDSAT8_NATIF_CAMS.zip?download=1

import os

def update_file(fpath, node, value):
    """
    Update a given XML file with
    :param fpath:
    :param node:
    :param value:
    :return:
    """
    from datetime import datetime
    from xml.etree import ElementTree
    it = ElementTree.iterparse(fpath)
    for _, el in it:
        if '}' in el.tag:
            el.tag = el.tag.split('}', 1)[1]  # strip all namespaces
    root = it.root
    modified = False
    for elem in root.findall(".//" + node):
        print("Updating %s: %s -> %s" % (node, elem.text, value))
        elem.text = value
        modified = True
    if modified:
        now = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
        for elem in root.findall(".//" + "Creation_Date"):
            elem.text = "UTC=" + now
    return root

def indent(elem, level=0):
    """
    Source: https://stackoverflow.com/questions/749796/pretty-printing-xml-in-python
    :param elem: The elem to indent
    :param level: The level of indentation
    :return: The indented element
    """
    i = "\n" + level * "  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level + 1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i
    return elem


def write_xml(root, filepath):
    """
    Write a pretty-printed xml to the given filepath.
    :param root: The root xml-Element
    :param filepath: The filepath to write it to.
    :return: Writes the given xml file with proper indentation.
    """
    from xml.etree import ElementTree
    assert os.path.isdir(os.path.dirname(filepath))
    indent(root)
    tree = ElementTree.ElementTree(root)
    tree.write(filepath, encoding="UTF-8", xml_declaration=True)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    parser.add_argument("node")
    parser.add_argument("value")
    args = parser.parse_args()

    write_xml(update_file(args.filename, args.node, args.value), args.filename)

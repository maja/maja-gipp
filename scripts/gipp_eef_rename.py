import os

import argparse
from shutil import copyfile



def update_EEF_files(input_dir, output_dir):
    print(input_dir, output_dir)
    files_EEF = [f for f in os.listdir(input_dir) if f.endswith('.EEF')]

    for i in range(0, len(files_EEF)-1):
        a = files_EEF[i].split(".")
        b = [x for x in a[0].split("_") if x]

        b[len(b) - 1] = '21000101'
        b[len(b) - 2] = '20190626'
        b[len(b) - 3] = b[-3][0] + "0001"
        b[len(b) - 4] = 'ALLSITES'

        if b[0] == 'S2' :
            b[0]='S2_'

        c = "_".join(b) + "." + a[1]


        copyfile(os.path.join(input_dir, files_EEF[i]), os.path.join(output_dir, c))



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("indir")
    parser.add_argument("outdir")
    args = parser.parse_args()

    update_EEF_files(args.indir, args.outdir)

    os.system(
        "for file in `ls "+args.outdir +"| grep '.EEF'`; do a=${file%.*}; sed -i 's#<File_Name>\(.*\)</File_Name>#<File_Name>'$a'</File_Name>#' "+args.outdir+"$a.EEF; done")

    os.system(
        "for file in `ls " + args.outdir + "| grep '.EEF'`; do a=${file%.*}; sed -i 's#<Validity_Start>\(.*\)</Validity_Start>#<Validity_Start>UTC=2019-06-26T00:00:00</Validity_Start>#' " + args.outdir + "$a.EEF; done")

    os.system(
        "for file in `ls " + args.outdir + "| grep '.EEF'`; do a=${file%.*}; sed -i 's#<Validity_Stop>\(.*\)</Validity_Stop>#<Validity_Stop>UTC=2100-01-01T00:00:00</Validity_Stop>#' " + args.outdir + "$a.EEF; done")

    os.system(
        "for file in `ls " + args.outdir + "| grep '.EEF'`; do a=${file%.*}; sed -i 's#<Creation_Date>\(.*\)</Creation_Date>#<Creation_Date>UTC=2019-06-26T00:00:00</Creation_Date>#' " + args.outdir + "$a.EEF; done")


    os.system(
        "for file in `ls " + args.outdir + "| grep '.EEF'`; do a=${file%.*}; sed -i 's#<File_Version>\(.*\)</File_Version>#<File_Version>0001</File_Version>#' " + args.outdir + "$a.EEF; done")

    os.system(
        "for file in `ls " + args.outdir + "| grep '.EEF'`; do a=${file%.*}; sed -i 's#<Applicability_NickName>\(.*\)</Applicability_NickName>#<Applicability_NickName>ALLSITES</Applicability_NickName>#' " + args.outdir + "$a.EEF; done")



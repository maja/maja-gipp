* GIPP (parameters) for MAJA
This directory corresponds to the parameters for
- MAJA 3.3.x, standalone version
- with MUSCATE format
- with CAMS option enabled
- for Venus satellite

For data volume reasons, the directory is not complete, and you should add the files contained in this reference :
https://zenodo.org/record/4590088/files/20200531_LUT_MAJA_VENUS_MUSCATE_CAMS.zip?download=1

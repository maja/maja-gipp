* GIPP (parameters) for MAJA
This directory corresponds to the parameters for
- MAJA 3.3.x, standalone version
- with MUSCATE format
- with CAMS option disabled
- for Sentinel-2 A&B satellites

For data volume reasons, the directory is not complete, and you should add the files contained in this reference :
https://zenodo.org/record/3891605/files/20200531_LUT_MAJA_SENTINEL2_MUSCATE.zip?download=1
